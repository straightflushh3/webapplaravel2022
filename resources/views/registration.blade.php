<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <h1>Registration</h1>
    <form action="/signup" method="POST">
        @csrf
        <input type="text" name="first_name"> - First Name
        <br><br>
        <input type="text" name="last_name"> - Last Name
        <br><br>
        <input type="text" name="email"> - Email
        <br><br>
        <input type="text" name="id"> - ID
        <br><br>
        <button>Register</button>

    </form>
</body>
</html>